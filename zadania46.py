class Vehicle:
    def __init__(self, name, number_of_wheels, top_speed):
        self.name = name
        self.number_of_wheels = number_of_wheels
        self.top_speed = top_speed


class Car(Vehicle):
    def print_parameters(self):
        print("%s has %d wheels and top speed is %d " % (self.name, self.number_of_wheels, self.top_speed))


class Mammal:
    def birthing(self) -> None:
        print("I am birthing")


class Dog(Mammal):
    pass


class Instrument:
    def __init__(self):
        print("I sound like this")


class Trumpet(Instrument):
    def __init__(self):
        super(Trumpet, self).__init__()
        print("tutututudutudu")


# Inheritance of variables check
car = Car("Audi", 4, 200)
car.print_parameters()

# Inheritance of methods check
dog = Dog()
dog.birthing()


# Overriding of methods check
trumpet = Trumpet()

