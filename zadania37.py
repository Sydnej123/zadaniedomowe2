def print_my_name() -> None:
    print("Filip Sobański")


def add_three_numbers(first_number, second_number, third_number) -> None:
    sum = first_number + second_number + third_number
    print(sum)


def hello_world() -> str:
    return "Hello World!"


def subtraction(first_number, second_number) -> int:
    sub = first_number - second_number
    return sub


class MyClass:
    pass


def return_object() -> MyClass:
    return MyClass()


# First function check
print_my_name()

# Second function check
add_three_numbers(4, 8, 10)

# Third function check
print(hello_world())

# Fourth function check
print(subtraction(22, 100))

# Fifth function check
my_class_object = return_object()
print(isinstance(my_class_object, MyClass))

